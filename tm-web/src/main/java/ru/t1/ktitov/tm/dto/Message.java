package ru.t1.ktitov.tm.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {

    private String value;

    public Message(String value) {
        this.value = value;
    }

}
