package ru.t1.ktitov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktitov.tm.dto.CustomUser;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ProjectService;
import ru.t1.ktitov.tm.service.TaskService;

import java.util.Collection;

@Controller
public final class TasksController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @ModelAttribute("projects")
    public Collection<Project> getProjects(@AuthenticationPrincipal final CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal final CustomUser user) {
        return new ModelAndView("task-list", "tasks", taskService.findAll(user.getUserId()));
    }

}
