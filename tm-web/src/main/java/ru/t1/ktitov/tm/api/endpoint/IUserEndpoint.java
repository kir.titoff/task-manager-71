package ru.t1.ktitov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/auth")
public interface IUserEndpoint {

    @WebMethod
    @PutMapping("/save")
    void save(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    );

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    );

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<User> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    User findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

}
