package ru.t1.ktitov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ProjectService;
import ru.t1.ktitov.tm.util.UserUtil;

import java.util.Arrays;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Project project1 = new Project("Test Project 1");

    private final Project project2 = new Project("Test Project 2");

    private final Project project3 = new Project("Test Project 3");

    private final Project project4 = new Project("Test Project 4");

    private Authentication authAdmin;

    private Authentication authTest;

    @Before
    public void init() {
        projectRepository.deleteAll();

        @NotNull UsernamePasswordAuthenticationToken tokenAdmin =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        authAdmin = authenticationManager.authenticate(tokenAdmin);

        @NotNull UsernamePasswordAuthenticationToken tokenTest =
                new UsernamePasswordAuthenticationToken("test", "test");
        authTest = authenticationManager.authenticate(tokenTest);

        SecurityContextHolder.getContext().setAuthentication(authTest);
        projectService.add(UserUtil.getUserId(), project1);
        projectService.add(UserUtil.getUserId(), project2);
    }

    @After
    public void clear() {
        projectRepository.deleteAll();
    }

    @Test
    public void addTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.add(UserUtil.getUserId(), project3);
        Assert.assertEquals(3, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void addAsListTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.add(UserUtil.getUserId(), Arrays.asList(project3, project4));
        Assert.assertEquals(4, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void updateTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals("Test Project 1",
                projectService.findOneById(UserUtil.getUserId(), project1.getId()).getName());

        project1.setName("Project 1");
        projectService.update(UserUtil.getUserId(), project1);
        Assert.assertEquals("Project 1",
                projectService.findOneById(UserUtil.getUserId(), project1.getId()).getName());
    }

    @Test
    public void findAllTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.add(UserUtil.getUserId(), project3);
        Assert.assertEquals(3, projectService.findAll(UserUtil.getUserId()).size());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        projectService.add(UserUtil.getUserId(), project4);
        Assert.assertEquals(1, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(project1.getId(), projectService.findOneById(UserUtil.getUserId(), project1.getId()).getId());
        Assert.assertNull(projectService.findOneById(UserUtil.getUserId(), project3.getId()));
    }

    @Test
    public void removeTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.remove(UserUtil.getUserId(), project1);
        Assert.assertEquals(1, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.removeById(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(1, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeAllTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, projectService.findAll(UserUtil.getUserId()).size());

        projectService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void createTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Project project = projectService.create(UserUtil.getUserId(), "Test Project 5");
        Assert.assertEquals(3, projectService.findAll(UserUtil.getUserId()).size());
        Assert.assertEquals(project.getId(), projectService.findOneById(UserUtil.getUserId(), project.getId()).getId());
    }

}
