package ru.t1.ktitov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.configuration.WebApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.util.UserUtil;

import javax.transaction.Transactional;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
public class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3");

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        add(project1);
        add(project2);
    }

    @After
    @SneakyThrows
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final Project project) {
        @NotNull final String url = BASE_URL + "save";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private Project findById(@NotNull final String id) {
        @NotNull final String url = BASE_URL + "find/" + id;
        @NotNull final String json = mockMvc
                .perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, Project.class);
    }

    @SneakyThrows
    private List<Project> findAll() {
        @NotNull final String url = BASE_URL + "findAll";
        @NotNull final String json = mockMvc
                .perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, List.class);
    }

    @Test
    public void saveTest() {
        add(project3);
        @Nullable final Project project = findById(project3.getId());
        Assert.assertEquals(project3.getId(), project.getId());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        Assert.assertNotNull(findById(project1.getId()));
        @NotNull final String url = BASE_URL + "delete";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(project1);
        mockMvc.perform(MockMvcRequestBuilders.delete(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        Assert.assertNotNull(findById(project1.getId()));
        @NotNull final String url = BASE_URL + "delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("utf-8"))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteAllTest() {
        Assert.assertEquals(2, findAll().size());
        @NotNull final String url = BASE_URL + "deleteAll";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
        add(project3);
        Assert.assertEquals(3, findAll().size());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        Assert.assertEquals(project1.getId(), findById(project1.getId()).getId());
        Assert.assertEquals(project1.getName(), findById(project1.getId()).getName());
    }

}
