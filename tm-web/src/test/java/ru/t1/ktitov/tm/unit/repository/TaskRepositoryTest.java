package ru.t1.ktitov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.util.UserUtil;

import javax.transaction.Transactional;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Task task1 = new Task();

    private final Task task2 = new Task();

    private final Task task3 = new Task();

    private final Task task4 = new Task();

    private final Task adminTask = new Task();

    private Authentication authAdmin;

    private Authentication authTest;

    @Before
    public void init() {
        taskRepository.deleteAll();

        @NotNull UsernamePasswordAuthenticationToken tokenAdmin =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        authAdmin = authenticationManager.authenticate(tokenAdmin);
        @NotNull UsernamePasswordAuthenticationToken tokenTest =
                new UsernamePasswordAuthenticationToken("test", "test");
        authTest = authenticationManager.authenticate(tokenTest);

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        adminTask.setUserId(UserUtil.getUserId());
        taskRepository.save(adminTask);

        SecurityContextHolder.getContext().setAuthentication(authTest);
        taskRepository.deleteByUserId(UserUtil.getUserId());
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
        taskRepository.save(task3);
        taskRepository.save(task4);

    }

    @After
    public void clean() {
        taskRepository.deleteAll();
    }

    @Test
    public void findFirstByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Task foundTask1 = taskRepository.findFirstById(task1.getId());
        Assert.assertEquals(task1.getId(), foundTask1.getId());
        @NotNull final Task foundTask2 = taskRepository.findFirstById(task2.getId());
        Assert.assertEquals(task2.getId(), foundTask2.getId());
        @NotNull final Task foundTask3 = taskRepository.findFirstById(task3.getId());
        Assert.assertEquals(task3.getId(), foundTask3.getId());
        @NotNull final Task foundTask4 = taskRepository.findFirstById(task4.getId());
        Assert.assertEquals(task4.getId(), foundTask4.getId());
    }

    @Test
    public void findByUserIdAndIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Task foundTask1 = taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(task1.getId(), foundTask1.getId());
        @NotNull final Task foundTask2 = taskRepository.findByUserIdAndId(UserUtil.getUserId(), task2.getId());
        Assert.assertEquals(task2.getId(), foundTask2.getId());
        @NotNull final Task foundTask3 = taskRepository.findByUserIdAndId(UserUtil.getUserId(), task3.getId());
        Assert.assertEquals(task3.getId(), foundTask3.getId());
        @NotNull final Task foundTask4 = taskRepository.findByUserIdAndId(UserUtil.getUserId(), task4.getId());
        Assert.assertEquals(task4.getId(), foundTask4.getId());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        @NotNull final Task foundTask = taskRepository.findByUserIdAndId(UserUtil.getUserId(), adminTask.getId());
        Assert.assertEquals(adminTask.getId(), foundTask.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    public void findByUserIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, taskRepository.findByUserId(UserUtil.getUserId()).size());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        Assert.assertEquals(1, taskRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, taskRepository.findByUserId(UserUtil.getUserId()).size());

        taskRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, taskRepository.findByUserId(UserUtil.getUserId()).size());

        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertEquals(3, taskRepository.findByUserId(UserUtil.getUserId()).size());
        Assert.assertNull(taskRepository.findFirstById(task1.getId()));
    }
    
}
