package ru.t1.ktitov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.util.UserUtil;

import javax.transaction.Transactional;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Project project1 = new Project();

    private final Project project2 = new Project();

    private final Project project3 = new Project();

    private final Project project4 = new Project();

    private final Project adminProject = new Project();

    private Authentication authAdmin;

    private Authentication authTest;

    @Before
    public void init() {
        projectRepository.deleteAll();

        @NotNull UsernamePasswordAuthenticationToken tokenAdmin =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        authAdmin = authenticationManager.authenticate(tokenAdmin);
        @NotNull UsernamePasswordAuthenticationToken tokenTest =
                new UsernamePasswordAuthenticationToken("test", "test");
        authTest = authenticationManager.authenticate(tokenTest);

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        adminProject.setUserId(UserUtil.getUserId());
        projectRepository.save(adminProject);

        SecurityContextHolder.getContext().setAuthentication(authTest);
        projectRepository.deleteByUserId(UserUtil.getUserId());
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        project3.setUserId(UserUtil.getUserId());
        project4.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
        projectRepository.save(project3);
        projectRepository.save(project4);

    }

    @After
    public void clean() {
        projectRepository.deleteAll();
    }

    @Test
    public void findFirstByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Project foundProject1 = projectRepository.findFirstById(project1.getId());
        Assert.assertEquals(project1.getId(), foundProject1.getId());
        @NotNull final Project foundProject2 = projectRepository.findFirstById(project2.getId());
        Assert.assertEquals(project2.getId(), foundProject2.getId());
        @NotNull final Project foundProject3 = projectRepository.findFirstById(project3.getId());
        Assert.assertEquals(project3.getId(), foundProject3.getId());
        @NotNull final Project foundProject4 = projectRepository.findFirstById(project4.getId());
        Assert.assertEquals(project4.getId(), foundProject4.getId());
    }

    @Test
    public void findByUserIdAndIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Project foundProject1 = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(project1.getId(), foundProject1.getId());
        @NotNull final Project foundProject2 = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project2.getId());
        Assert.assertEquals(project2.getId(), foundProject2.getId());
        @NotNull final Project foundProject3 = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project3.getId());
        Assert.assertEquals(project3.getId(), foundProject3.getId());
        @NotNull final Project foundProject4 = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project4.getId());
        Assert.assertEquals(project4.getId(), foundProject4.getId());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        @NotNull final Project foundProject = projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), adminProject.getId());
        Assert.assertEquals(adminProject.getId(), foundProject.getId());
        Assert.assertNull(projectRepository.findFirstByUserIdAndId(UserUtil.getUserId(), project1.getId()));
    }

    @Test
    public void findByUserIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, projectRepository.findByUserId(UserUtil.getUserId()).size());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        Assert.assertEquals(1, projectRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, projectRepository.findByUserId(UserUtil.getUserId()).size());

        projectRepository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectRepository.findByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(4, projectRepository.findByUserId(UserUtil.getUserId()).size());

        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project1.getId());
        Assert.assertEquals(3, projectRepository.findByUserId(UserUtil.getUserId()).size());
        Assert.assertNull(projectRepository.findFirstById(project1.getId()));
    }

}
