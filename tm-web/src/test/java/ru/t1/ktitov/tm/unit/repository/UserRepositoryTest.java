package ru.t1.ktitov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.User;

import javax.transaction.Transactional;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserRepositoryTest {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    private final User user1 = new User();

    private final User user2 = new User();

    @Before
    public void init() {
        userRepository.deleteAll();
        user1.setLogin("admin");
        user1.setPasswordHash(passwordEncoder.encode("admin"));
        userRepository.save(user1);
        user2.setLogin("test");
        user2.setPasswordHash(passwordEncoder.encode("test"));
        userRepository.save(user2);
    }

    @After
    public void clean() {
        userRepository.deleteAll();
    }

    @Test
    public void findByLoginTest() {
        @Nullable final User foundUser1 = userRepository.findByLogin(user1.getLogin());
        Assert.assertEquals(user1.getId(), foundUser1.getId());
        @Nullable final User foundUser2 = userRepository.findByLogin(user2.getLogin());
        Assert.assertEquals(user2.getId(), foundUser2.getId());
    }

    @Test
    public void findFirstByIdTest() {
        @Nullable final User foundUser1 = userRepository.findFirstById(user1.getId());
        Assert.assertEquals(user1.getId(), foundUser1.getId());
        @Nullable final User foundUser2 = userRepository.findFirstById(user2.getId());
        Assert.assertEquals(user2.getId(), foundUser2.getId());
    }

}
