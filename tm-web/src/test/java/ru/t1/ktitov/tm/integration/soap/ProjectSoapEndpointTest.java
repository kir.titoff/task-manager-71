package ru.t1.ktitov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.client.AuthSoapEndpointClient;
import ru.t1.ktitov.tm.client.ProjectSoapEndpointClient;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    private final static String BASE_URL = "http://localhost:8080";

    @NotNull
    private final Project project1 = new Project("Project1");

    @NotNull
    private final Project project2 = new Project("Project2");

    @NotNull
    private final Project project3 = new Project("Project3");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(BASE_URL);
        Assert.assertTrue(authEndpoint.login("test", "test").isSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(BASE_URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        Map<String, List<String>> headers = CastUtils
                .cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        projectEndpoint.save(project1);
        projectEndpoint.save(project2);
    }

    @After
    public void clean() {
        projectEndpoint.deleteAll();
    }

    @Test
    public void addTest() {
        projectEndpoint.save(project3);
        Assert.assertNotNull(projectEndpoint.findById(project3.getId()));
    }

    @Test
    public void deleteTest() {
        projectEndpoint.delete(project1);
        Assert.assertNull(projectEndpoint.findById(project1.getId()));
    }

    @Test
    public void deleteByIdTest() {
        projectEndpoint.deleteById(project1.getId());
        Assert.assertNull(projectEndpoint.findById(project1.getId()));
    }

    @Test
    public void deleteAllTest() {
        projectEndpoint.deleteAll();
        Assert.assertNull(projectEndpoint.findById(project1.getId()));
        Assert.assertNull(projectEndpoint.findById(project2.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectEndpoint.findAll().size());
    }

    @Test
    public void findByIdTest() {
        Assert.assertNotNull(projectEndpoint.findById(project1.getId()));
        Assert.assertNotNull(projectEndpoint.findById(project2.getId()));
        Assert.assertNull(projectEndpoint.findById(project3.getId()));
    }

}
