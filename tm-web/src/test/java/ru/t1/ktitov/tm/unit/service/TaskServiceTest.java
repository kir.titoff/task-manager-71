package ru.t1.ktitov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.service.TaskService;
import ru.t1.ktitov.tm.util.UserUtil;

import java.util.Arrays;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private TaskService taskService;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final Task Task1 = new Task("Test Task 1");

    private final Task Task2 = new Task("Test Task 2");

    private final Task Task3 = new Task("Test Task 3");

    private final Task Task4 = new Task("Test Task 4");

    private Authentication authAdmin;

    private Authentication authTest;

    @Before
    public void init() {
        taskRepository.deleteAll();

        @NotNull UsernamePasswordAuthenticationToken tokenAdmin =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        authAdmin = authenticationManager.authenticate(tokenAdmin);

        @NotNull UsernamePasswordAuthenticationToken tokenTest =
                new UsernamePasswordAuthenticationToken("test", "test");
        authTest = authenticationManager.authenticate(tokenTest);

        SecurityContextHolder.getContext().setAuthentication(authTest);
        taskService.add(UserUtil.getUserId(), Task1);
        taskService.add(UserUtil.getUserId(), Task2);
    }

    @After
    public void clear() {
        taskRepository.deleteAll();
    }

    @Test
    public void addTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());

        taskService.add(UserUtil.getUserId(), Task3);
        Assert.assertEquals(3, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void addAsListTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());

        taskService.add(UserUtil.getUserId(), Arrays.asList(Task3, Task4));
        Assert.assertEquals(4, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void updateTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals("Test Task 1",
                taskService.findOneById(UserUtil.getUserId(), Task1.getId()).getName());

        Task1.setName("Task 1");
        taskService.update(UserUtil.getUserId(), Task1);
        Assert.assertEquals("Task 1",
                taskService.findOneById(UserUtil.getUserId(), Task1.getId()).getName());
    }

    @Test
    public void findAllTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());

        taskService.add(UserUtil.getUserId(), Task3);
        Assert.assertEquals(3, taskService.findAll(UserUtil.getUserId()).size());

        SecurityContextHolder.getContext().setAuthentication(authAdmin);
        taskService.add(UserUtil.getUserId(), Task4);
        Assert.assertEquals(1, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findOneByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(Task1.getId(), taskService.findOneById(UserUtil.getUserId(), Task1.getId()).getId());
        Assert.assertNull(taskService.findOneById(UserUtil.getUserId(), Task3.getId()));
    }

    @Test
    public void removeTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());

        taskService.remove(UserUtil.getUserId(), Task1);
        Assert.assertEquals(1, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByIdTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        Assert.assertEquals(2, taskService.findAll(UserUtil.getUserId()).size());

        taskService.removeById(UserUtil.getUserId(), Task1.getId());
        Assert.assertEquals(1, taskService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void createTest() {
        SecurityContextHolder.getContext().setAuthentication(authTest);
        @NotNull final Task Task = taskService.create(UserUtil.getUserId(), "Test Task 5");
        Assert.assertEquals(3, taskService.findAll(UserUtil.getUserId()).size());
        Assert.assertEquals(Task.getId(), taskService.findOneById(UserUtil.getUserId(), Task.getId()).getId());
    }
    
}
