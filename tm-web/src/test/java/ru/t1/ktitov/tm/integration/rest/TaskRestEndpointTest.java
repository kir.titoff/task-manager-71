package ru.t1.ktitov.tm.integration.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.ktitov.tm.dto.Result;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.Task;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    private static String SESSION_ID;

    @NotNull
    private final static String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Task task1 = new Task("Task1");

    @NotNull
    private final Task task2 = new Task("Task2");

    @NotNull
    private final Task task3 = new Task("Task3");

    @NotNull
    private final Task task4 = new Task("Task4");

    @NotNull
    private static final HttpHeaders HEADERS = new HttpHeaders();

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url =
                "http://localhost:8080/api/auth/getlogin?username=test&password=test";
        @NotNull final ResponseEntity<Result> response =
                restTemplate.getForEntity(url, Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().isSuccess());
        @NotNull HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = java.net.HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        SESSION_ID = cookies.stream()
                .filter(
                        item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADERS.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADERS));
    }

    private static ResponseEntity<Task> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, Task.class);
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    @Before
    public void init() {
        @NotNull final String url = BASE_URL + "save";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task1, HEADERS));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task2, HEADERS));
    }

    @After
    public void clean() {
        @NotNull final String url = BASE_URL + "deleteAll";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
    }

    @Test
    public void saveTest() {
        @NotNull final String saveUrl = BASE_URL + "save";
        sendRequest(saveUrl, HttpMethod.POST, new HttpEntity<>(task3, HEADERS));
        @NotNull final String findUrl = BASE_URL + "find/" + task3.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String deleteUrl = BASE_URL + "delete";
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        @NotNull final String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(task1);
        sendRequest(deleteUrl, HttpMethod.DELETE, new HttpEntity<>(json, HEADERS));
        @NotNull final String findUrl = BASE_URL + "find/" + task1.getId();
        Assert.assertNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull final String deleteUrl = BASE_URL + "delete/" + task1.getId();
        sendRequest(deleteUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
        @NotNull final String findUrl = BASE_URL + "find/" + task1.getId();
        Assert.assertNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

    @Test
    @SneakyThrows
    public void deleteAllTest() {
        @NotNull final String deleteUrl = BASE_URL + "deleteAll/";
        sendRequest(deleteUrl, HttpMethod.DELETE, new HttpEntity<>(HEADERS));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody().size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull String findUrl = BASE_URL + "find/" + task1.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
        findUrl = BASE_URL + "find/" + task2.getId();
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
        findUrl = BASE_URL + "find/" + task3.getId();
        Assert.assertNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADERS)).getBody());
    }

}
