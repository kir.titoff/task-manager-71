package ru.t1.ktitov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.enumerated.RoleType;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.Role;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.service.UserService;

import java.util.Arrays;
import java.util.Collections;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class UserServiceTest {

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    private final User user1 = new User();

    private final User user2 = new User();

    private final User user3 = new User();

    private final User user4 = new User();

    @Before
    public void init() {
        userRepository.deleteAll();
        userService.add(user1);
        userService.add(user2);
    }

    @After
    public void clear() {
        userRepository.deleteAll();
    }

    @Test
    public void addTest() {
        Assert.assertEquals(2, userService.findAll().size());

        userService.add(user3);
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test
    public void addAsListTest() {
        Assert.assertEquals(2, userService.findAll().size());

        userService.add(Arrays.asList(user3, user4));
        Assert.assertEquals(4, userService.findAll().size());
    }

    @Test
    public void updateTest() {
        final Role role = new Role();
        role.setUser(user1);
        role.setRoleType(RoleType.USER);
        user1.setRoles(Collections.singletonList(role));
        userService.update(user1);
        Assert.assertEquals(role.toString(), userService.findOneById(user1.getId()).getRoles().get(0).toString());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, userService.findAll().size());

        userService.add(user3);
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test
    public void findOneByIdTest() {
        Assert.assertEquals(user1.getId(), userService.findOneById(user1.getId()).getId());
        Assert.assertNull(userService.findOneById(user3.getId()));
    }

    @Test
    public void removeTest() {
        Assert.assertEquals(2, userService.findAll().size());

        userService.remove(user1);
        Assert.assertEquals(1, userService.findAll().size());
    }

    @Test
    public void removeByIdTest() {
        Assert.assertEquals(2, userService.findAll().size());

        userService.removeById(user1.getId());
        Assert.assertEquals(1, userService.findAll().size());
    }
    
}
