package ru.t1.ktitov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "User logout";

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        authEndpoint.logout(request);
        setToken(null);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
