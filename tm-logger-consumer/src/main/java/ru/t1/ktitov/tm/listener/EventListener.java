package ru.t1.ktitov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.service.LoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public final class EventListener implements MessageListener {

    @NotNull
    @Autowired
    private LoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final String json = ((TextMessage) message).getText();
        loggerService.log(json);
    }

}
