package ru.t1.ktitov.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.AbstractModel;

@Repository
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {
}
