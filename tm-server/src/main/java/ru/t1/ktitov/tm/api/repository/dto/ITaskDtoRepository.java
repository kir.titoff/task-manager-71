package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findByUserId(@NotNull final String userId);

    @Nullable
    List<TaskDTO> findByProjectId(@NotNull final String userId);

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId")
    TaskDTO findByIdAndUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    );

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId")
    List<TaskDTO> findByUserIdAndProjectId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("projectId") final String projectId
    );

    @Nullable
    @Query("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY :orderBy")
    List<TaskDTO> findAll(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("orderBy") final String orderBy
    );

    @Query("SELECT COUNT(1) = 1 FROM TaskDTO t WHERE t.id = :id AND t.userId = :userId")
    Boolean existsByIdAndUserId(
        @NotNull @Param("userId") final String userId,
        @NotNull @Param("id") final String id
    );

}
