package ru.t1.ktitov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {

    @Nullable
    List<SessionDTO> findByUserId(@NotNull final String userId);

    @Nullable
    @Query("SELECT s FROM SessionDTO s WHERE s.id = :id AND s.userId = :userId")
    SessionDTO findByIdAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

    @Query("SELECT COUNT(1) = 1 FROM SessionDTO s WHERE s.id = :id AND s.userId = :userId")
    Boolean existsByIdAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("id") final String id
    );

}
